import productsData from '../data/products';
import ProductCard from '../components/ProductCard';

export default function Products(){


	const product = productsData.map((product) => {
	
		return <ProductCard productProp={product} key={product.id}/>
	})

	return(
		<>
			{product}
		</>
	)
}
