import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useState } from 'react';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';

function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }




  return (
   <>
    <AppNavbar/>
    <Container>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/courses" element={<Products/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="/register" element={<Register/>}/>
        <Route path="/logout" element={<Logout/>}/>
      </Routes>

    </Container>
    </>

  );
}

export default App;
