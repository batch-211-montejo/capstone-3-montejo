import {Row, Col, Button} from 'react-bootstrap';

export default function Banner(){
	return (
		<Row>
			<Col>
				<h1>JUST fashion</h1>
				<p>be classy and fabulous!</p>
				<Button variant="primary">Buy now!</Button>
			</Col>
		</Row>
	)
}
