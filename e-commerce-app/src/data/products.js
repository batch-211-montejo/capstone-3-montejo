const productsData = [
	{
		id: "1",
		name: "jeans",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 1999,
		onOffer: true
	},
	{
		id: "2",
		name: "shirt",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 999,
		onOffer: true
	},
	{
		id: "3",
		name: "skirt",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 1499,
		onOffer: true
	}	
]

export default productsData;
