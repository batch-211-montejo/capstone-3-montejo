import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}){

	// console.log(props)

	let { name, description, price } = productProp;

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Button variant="primary">Buy</Button>
			</Card.Body>
		</Card>
	)
}
